from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest, HttpResponse
import pandas as pd
import csv
from file_editor.backend.csv_info import CSVMeta
from file_editor.models import CSVMetaModel
import uuid
from django.core import serializers
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from file_editor.forms import *
from file_editor.models import *
import operator


@login_required()
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            r = range(1, 5)
            user = request.user
            title = form.cleaned_data['title']
            columns = [form.cleaned_data['columns' + str(i)] for i in r]
            delimiters = [form.cleaned_data['delimiter' + str(i)] for i in r]
            line_ending = form.cleaned_data['line_ending']
            output_delimiter = form.cleaned_data['output_delimiter']
            project = Project(user=user, title=title, line_ending=line_ending, output_delimiter=output_delimiter)
            project.save()
            for col, delimiter in zip(columns, delimiters):
                csv_meta = CSVMetaModel(columns=col, delimiter=delimiter, project=project)
                csv_meta.save()
            return redirect('/upload')
        else:
            return HttpResponseBadRequest()
    else:
        form = CreateProjectForm()
        return render(request, 'create_project.html', {'form': form})


@login_required()
def view_projects(request):
    user = request.user
    projects = Project.objects.filter(user=user)
    return render(request, 'view_project.html', {'projects': projects})


@login_required()
def delete_project(request):
    pk = request.GET.get('pk')
    Project.objects.filter(pk=pk).delete()
    return redirect('/view')


@login_required()
def upload(request):
    if request.method == "POST":
        form = UpdateForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            project_pk = form.cleaned_data['project']
            project = Project.objects.get(pk=project_pk)
            meta_model = sorted(CSVMetaModel.objects.filter(project=project_pk), key=operator.attrgetter('pk'))
            r = range(1, 5)
            files = [request.FILES.get('file' + str(i), None) for i in r]
            columns = [meta.columns for meta in meta_model]
            delimiters = [meta.delimiter for meta in meta_model]
            result = []
            for f, col, delimiter in zip(files, columns, delimiters):
                selected = CSVMeta(f, col, delimiter).process()
                if selected is not None:
                    result.append(selected)
            big_df = pd.concat(result, axis=1)
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="output.csv"'
            lineterminator = project.line_ending
            writer = csv.writer(response, delimiter=str(project.output_delimiter),
                                lineterminator=lineterminator)
            for row in big_df.values:
                writer.writerow(row)
            return response
        else:
            return HttpResponseBadRequest()
    else:
        projects = Project.objects.filter(user=request.user)
        form = None
        no_projects = True
        if len(projects):
            form = UpdateForm(request.user)
            no_projects = False
        return render(request, 'upload_form.html', {'form': form, 'no_projects': no_projects})


def test_models(request):
    data = serializers.serialize("json", CSVMetaModel.objects.all())
    bytes = data.encode('utf-8')
    return HttpResponse(bytes, content_type='application/json')


def sign_in(request):
    if request.method == 'POST':
        username = request.POST['email']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/upload')
        else:
            return render(request, 'auth.html', {'incorrect': True})
    else:
        return render(request, 'auth.html', {'title': 'Login'})


def register(request):
    if request.method == 'POST':
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        username = request.POST['email']
        email = request.POST['email']
        password = request.POST['password']
        if not User.objects.filter(username=username):
            user = User.objects.create_user(first_name=firstname,
                                            last_name=lastname,
                                            username=username,
                                            email=email,
                                            password=password)
            user.save()
            return redirect('/login')
        else:
            return render(request, 'auth.html', {'register': True, 'incorrect': True})
    else:
        return render(request, 'auth.html', {'register': True})


def logout_view(request):
    logout(request)
    return redirect('/')


def index(request):
    if not request.user.is_authenticated():
        return render(request, 'index.html')
    else:
        form = UpdateForm(request.user)
        return render(request, 'upload_form.html', {'form': form})
