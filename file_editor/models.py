import pandas as pd
from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    line_ending = models.CharField(max_length=10)
    output_delimiter = models.CharField(max_length=2)


class CSVMetaModel(models.Model):
    delimiter = models.CharField(max_length=2)
    columns = models.CharField(max_length=200)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
