import pandas as pd


class CSVMeta:
    def __init__(self, file_obj, columns, delimiter):
        self.file_obj = file_obj
        self.delimiter = delimiter
        if columns is not None and columns is not u'':
            self.columns = map(int, columns.split(','))
        else:
            self.columns = []

    def process(self):
        if self.file_obj is not None:
            return pd.read_csv(self.file_obj, usecols=self.columns, delimiter=self.delimiter, header=None,
                               encoding='latin-1')
        else:
            return None
