from django import forms
from django.utils.safestring import mark_safe

from file_editor.models import Project


class UpdateForm(forms.Form):
    def __init__(self, user, *args, **kwargs):
        super(UpdateForm, self).__init__(*args, **kwargs)
        self.fields['project'] = forms.ChoiceField(
            choices=[(o.id, o.title) for o in Project.objects.filter(user=user)])

    file1 = forms.FileField(required=True, label=mark_safe('<br />File1'))
    file2 = forms.FileField(required=False, label=mark_safe('<br />File2'))
    file3 = forms.FileField(required=False, label=mark_safe('<br />File3'))
    file4 = forms.FileField(required=False, label=mark_safe('<br />File4'))


class CreateProjectForm(forms.Form):
    delimiter_choices = ((',', ','), (';', ';'))
    line_ending_choices = (('\r\n', 'Windows (CR+LF)'), ('\n', 'Linux (LF)'))
    title = forms.CharField(required=True, label='Project Title')

    columns1 = forms.CharField(required=False, label='Paste comma-separated columns for File 1')
    delimiter1 = forms.ChoiceField(choices=delimiter_choices)

    columns2 = forms.CharField(required=False, label='Paste comma-separated columns for File 2')
    delimiter2 = forms.ChoiceField(choices=delimiter_choices)

    columns3 = forms.CharField(required=False, label='Paste comma-separated columns for File 3')
    delimiter3 = forms.ChoiceField(choices=delimiter_choices)

    columns4 = forms.CharField(required=False, label='Paste comma-separated columns for File 4')
    delimiter4 = forms.ChoiceField(choices=delimiter_choices)

    line_ending = forms.ChoiceField(choices=line_ending_choices, label=mark_safe('Select line ending:'))

    output_delimiter = forms.ChoiceField(choices=delimiter_choices, label=mark_safe('Select output delimiter:'))
